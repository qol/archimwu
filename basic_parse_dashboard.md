### Configuration of the parse dashboard
 
 Must be stored in the same directory as the docker-compose.yml file with the following name "./parsedashboard_data/config.json".


``` json
{
  "apps": [
    {
      "serverURL": "https://YOUR_DOMAIN_NAME/parse",
      "appId": "YOUR_PARSE_APP_ID",
      "masterKey": "YOUR_PARSE_SERVER_MASTER_KEY",
      "appName": "YOUR_PARSE_APP_NAME",
      "supportedPushLocales": ["en","dk","fr","ch","es"]
    }
  ],
"users": [
    {
      "user":"USER_NAME",
      "pass":"PASSWORD"
    }
],
"trustProxy": 1
}
```
