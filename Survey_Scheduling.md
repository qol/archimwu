
The below JSON shows a simple but practical format to specify the
scheduling of fixed-time and variable-time surveys. In our case, we
define one of such specifications for each study, and a backend application
in Ruby schedules the Push Notification jobs using sidekiq for each 
study participant .

``` json

{
  "fixed": {
    "Survey Name - optional": {
      "days": {
        "mon": false,
        "tue": false,
        "wed": false,
        "thu": false,
        "fri": true,
        "sat": false,
        "sun": false
      },
      "info": {
        "prompt_hour": 10,
        "survey": "a survey id",
        "weeklyFrequency": 2   
      }
    }
  },
  "random": {
    "Survey Name - optional": {
      "days": {
        "mon": true,
        "tue": true,
        "wed": true,
        "thu": true,
        "fri": true,
        "sat": true,
        "sun": true
      },
      "info": {
        "start_hour": 8,
        "end_hour": 20,
        "num_prompts": 4,
        "survey": "a survey id",
        "weeklyFrequency": 1    
      }
    },
    "Survey Name - optional": {
      "days": {
        "mon": true,
        "tue": true,
        "wed": true,
        "thu": true,
        "fri": true,
        "sat": true,
        "sun": true
      },
      "info": {
        "start_hour": 17,
        "end_hour": 18,
        "num_prompts": 1,
        "survey": "a survey id",
        "weeklyFrequency": 1
      }
    }
  }
}
```
