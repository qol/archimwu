# ArchIMWU

This is a public repository to complement the following conference paper. 

Allan Berrocal, Vlad Manea, Alexandre De Masi, and Katarzyna Wac. “mQoL Lab: Step- by-step creation of a flexible platform to conduct studies using interactive, mobile, wearable and ubiquitous devices.” Procedia Computer Science, 175: 221-229, August 2020b. ISSN 1877-0509. [doi:10.1016/j.procs.2020.07.033](https://doi.org/10.1016/j.procs.2020.07.033). Proceedings of the 17th International Conference on Mobile Systems and Pervasive Computing (MobiSPC). 

The following files are available in this repository :
- docker-compose : 
- nginx configuration for hosting the applications
- parse dashboard configuration 
- cloud code basic function for the application
- survey scheduling file structure
