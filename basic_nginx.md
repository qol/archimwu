
#### Configuration of the nginx server

Must be stored in the same directory as the docker-compose.yml file with the following name  ./nginx_data/app.conf



``` nginx
server {
    listen 80;
    server_name your_domain_name;
    location / {
        return 301 https://$host$request_uri;
    }    
}

server {
    listen 443 ssl;
    server_name your_domain_name;
    root /usr/share/nginx/html;
    index index.html index.htm;

    ssl_certificate PATH_TO_YOUR/fullchain.pem;
	ssl_certificate_key PATH_TO_YOUR/privkey.pem;
	include PATH_TO_YOUR/options-ssl-nginx.conf;
	ssl_dhparam PATH_TO_YOUR/ssl-dhparams.pem;
	

	location /.well-known/acme-challenge/ {   # Accesible challenge for renewing the SSL certificate 
		root /var/www/certbot;
	}
	
	location /parseserver/ {
        resolver 127.0.0.11;    # ip address of the Docker DNS, hence parseserver 
                                # (docker container's name) can be use directly
                                # in the nginx configuration file.
        
        proxy_pass http://parseserver:1337/parse/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
    }
        
    location /parsedashboard/ {
        resolver 127.0.0.11;
        proxy_pass http://parsedashboard:4040/dash/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
    }
    
    location /webapp/ {
        resolver 127.0.0.11;
        proxy_pass http://webapp:3000/;
        proxy_set_header Host $host;
        proxy_redirect off;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header  X-Forwarded-Proto $scheme;
        proxy_set_header  X-Forwarded-Ssl on; # Optional
        proxy_set_header  X-Forwarded-Port $server_port;
        proxy_set_header  X-Forwarded-Host $host;
    }
    
}
```
