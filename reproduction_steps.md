# Steps to Reproduce the Architecture

This text outlines the steps required to reproduce the components of our architectural design (Fig 3 of the paper). 
Researchers can follow these steps to bootstrap a solid foundation for their own platform. We recommend the following steps:

## (1) Provisioning the server components

1.  Server components are instantiated as Docker images in containers. They can be assembled using the Docker Compose utility (https://docs.docker.com/compose/). We provide the file `docker-compose.yml` with the settings for the images. 
2.  Server components are split in three (recommended) groups: (G1) `parseserver`, `parsedashboard`, and `mongoDB`, since these form the core of the server required to develop mobile clients; (G2) `certbot` and `nginx`, as these form the networking and security layer of the backend; and (G3) `rails`, `redis`, and `sidekiq`, because these groups are optional. 
3.  As soon as G1 is created, researchers can already start interacting with the `Parse Dashboard` and `Cloud Code` features. 

## (2) Provisioning the client components

1.  Mobile applications: we assume research labs implement their own mobile applications (e.g., `Android` and `iOS`) or use open source alternatives (e.g., those listed in section 2.2 of the paper. The main strength of `Parse Server` is in its SDKs which allow native integrations with those clients. Alternatively, researchers using hybrid frameworks can leverage the `Parse Server` `JavaScript` or REST API libraries. 
2.  Wearable devices: can be integrated depending on the type of device and the interfaces provided by its manufacturer (see Subsection~\ref{subsec:arch_design_zoom}).
3.  Web clients: as part of their corresponding web applications, web clients facilitate operations by participants, researchers, and other mobile applications in the platform. `Parse Server` offers numerous libraries to integrate the web clients to the servers. Participant-facing web applications enable study registration, informed consent collection, visualizations, and potentially technical support. Researcher-facing web applications can enable study design, participants management, and data analysis. 

## (3) Recommendations for reproducing the platform

1. We recommend an incremental and iterative deployment process, by keeping in mind the robust and flexible layers of the conceptual model, which separates features into core and transient Fig. 2 of the paper. During deployment, we advise that core features shall be deployed first.
2. We recommend the distribution mobile applications via the official `Android` or `iOS` stores, so that participants can use their own IMWU devices. When not possible, devices can be manually provisioned and offered to participants for the study.
3. We recommend the use of separate development, staging, and production environments which can be accomplished by instantiating the server components and redirecting the clients to the desired server. One way is to use (local) version control branches to track the differences between these environments.   
4. We recommend the adoption of software engineering practices such as planning, documentation, testing, and versioning to improve maintainability of the platform in a changing team of researchers and simplify its evolution towards becoming a robust and flexible software ecosystem.
