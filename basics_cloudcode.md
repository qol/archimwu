This illustrates the basic components of application logic hosted in the Parse Cloud 

In general, a cloud function has the below format.

``` javascript
Parse.Cloud.define('myCloudFunction', (request) => {

  /*
    For certain operations in parse objects you need to use the master key
    An example is shown below
  */
	var myParam = request.params.myParam;

	if (myParam) {
		  // set masterkey option
    	var options = {useMasterKey: true};

    	// 2. Get the Parse user by username (Google ID)
    	return findUserBasedOnParam(myParam, options).then((user) => {

    		if (user) {
          // Operations with the retrieved object

    		}
    		else {
          // Object does not exist  
    		}
    	});    	
	}
	else {
		request.log.error("Missing required params.");
		throw "Missing required params.";
	}
});


/*
 *
 * Sample function showing a query to a parse object 
 * using the masterkey
 *
*/
function findUserBasedOnParam(userName, options) {

	return new Promise((resolve, reject) => {
      var queryUser = new Parse.Query(Parse.User);
      queryUser.equalTo("username", userName);

      return queryUser.first(options)
          .then((user) => {
              console.log("Success: Found user by name")
              resolve(user)
          })
          .catch((error) => {
              console.log("Error: Did not find user by name "+ error.message)
              reject(error)
          });
	});
}

/*
 *
 * This function can be used to send push notifications from the server
 * Andoid or iOS clients will call it when needed.
 *
*/
Parse.Cloud.define('sendPushNotification', async (request) => {
  var params = request.params;
  
  var options = {useMasterKey: true};         

  if (!params) {
      console.log("Missing required params");
      return "Missing required params";    
  } else {
      var p1  = JSON.parse(params).channel;           // e.g. intended channel for the Push for multiple targets
      var p2  = JSON.parse(params).message;           // e.g. message
    
      await Parse.Push.send({
          channels: [p1],
          data: {
              "content-available":1,              // for iOS
              "message":p2                        // custom message as needed
          }
          }, options)
          .then(() => {
              console.log("PUSH was sent to matching clients.");
              return "PUSH was sent to matching clients.";
          })
          .catch((error) => {
              console.log("Push Send error: " + error.message);
              return "Push Send error.";
          });     
  }
});

```

